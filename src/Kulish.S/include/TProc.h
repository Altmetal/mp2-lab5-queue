#include "tqueue.h"
#include "tjobstream.h"

#include <string>

class TProc
{
private:
	TJobStream jobStream;
	TQueue job;
	bool isUnLocked;
	unsigned takts;
	unsigned jobCount, refusingCount, downtimeCount;
	int inProc;
public:
	TProc(unsigned short, unsigned short, unsigned, unsigned);
	void service(void);
	void printResult(void);
};