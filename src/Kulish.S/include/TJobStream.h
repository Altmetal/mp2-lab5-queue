
class TJobStream
{
private:
	unsigned ID;
	unsigned short q1, q2;
public:
	TJobStream(unsigned short, unsigned short);
	unsigned creatureNewJob(void);
	bool endJob(void);
	unsigned getID(void);
};