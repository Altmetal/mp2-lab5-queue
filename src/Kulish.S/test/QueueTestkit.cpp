
#include "gtest.h"
#include "tqueue.h"


TEST(TQueue, Can_Fill_Up_To_A_Maximum)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	EXPECT_EQ(Q.GetCount(), N);
}

TEST(TQueue, Can_Fill_Up_Not_To_Max)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N - 1; i++)
		Q.Put(i);
	EXPECT_NE(Q.GetCount(), N);
}


TEST(TQueue, Can_Remove_Elements)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	for (int i = 0; i<N; i++)
		EXPECT_EQ(Q.get(), i);
}

TEST(TQueue, Cyclical_Buf_Works)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	for (int i = 0; i<N - 1; i++)
		EXPECT_EQ(Q.get(), i);
	for (int i = 0; i<N - 1; i++)
		Q.Put(N - 1);
	for (int i = 0; i<N; i++)
		EXPECT_EQ(Q.get(), N - 1);
}

TEST(TQueue, Memory_Is_Empty)
{
	const int N = 3;
	TQueue Q(0);
	Q.get();
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
	Q.Put(1);
	EXPECT_EQ(DataNoMem, Q.GetRetCode());
}

TEST(TQueue, Everything_Is_Allright)
{
	const int N = 3;
	TQueue Q(N);
	for (int i = 0; i<N; i++)
		Q.Put(i);
	Q.get();
	Q.Put(1);
	EXPECT_EQ(DataOK, Q.GetRetCode());
}

TEST(TQueue, Can_Create_Queue_With_Positive_Length)
{
	ASSERT_NO_THROW(TQueue queue(10));
}

TEST(TQueue, Throws_When_Create_Queue_With_Negative_Length)
{
	ASSERT_ANY_THROW(TQueue queue(-10));
}

TEST(TQueue, IsEmpty_Works)
{
	TQueue queue;

	ASSERT_TRUE(queue.IsEmpty());
}

TEST(TQueue, IsFull_Works)
{
	TQueue queue(1);

	queue.Put(10);

	ASSERT_TRUE(queue.IsFull());
}

TEST(TQueue, Cant_Put_In_Full_Queue)
{
	int id = 0;
	unsigned int size = 10;
	TQueue Q(size);
	for (unsigned int i = 0; i < size; i++)
		Q.Put(i);
	Q.Put(id);
	EXPECT_EQ(DataFull, Q.GetRetCode());
}
