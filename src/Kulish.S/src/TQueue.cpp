#include "TQueue.h"

TQueue::TQueue(TData size) :TStack(size), Li(0){}

TData TQueue::get(void)
{
	TData result = -1;
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			result = pMem[(Li++) % MemSize];
			DataCount--;
		}
	return result;
}