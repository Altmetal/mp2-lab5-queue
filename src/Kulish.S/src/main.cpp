#include <gtest/gtest.h>
#include "TProc.h"


int main(int argc, char **argv)
{
  /*::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();*/

	unsigned tacts, sizeQ;
	unsigned short q1, q2;


  cout << "Enter the number of cycles" << endl;
  cin >> tacts;
  cout << "Enter the size of the queue" << endl;
  cin >> sizeQ;
  cout << "Enter the regulatory tasks flow rate (0 to 99)" << endl;
  cin >> q1;
  cout << "Enter the processor performance computing system (0 to 99)" << endl;
  cin >> q2;

  TProc P(q1, q2, sizeQ, tacts);
  for (int i = 0; i<tacts; i++)
    P.service();
  P.printResult();               
}

