
#include "tproc.h"
#include <iostream>

using namespace std;

TProc::TProc(unsigned short _q1, unsigned short _q2, unsigned _size, unsigned _takts) : jobStream(_q1, _q2), job(_size), takts(_takts)
{
	isUnLocked = true;
	jobCount = 0;
	refusingCount = 0;
	downtimeCount = 0;
}

void TProc::service(void)
{
	int Id;

	if (Id = jobStream.creatureNewJob())
	{
		job.Put(Id);

		if (job.GetRetCode() == DataFull)
			refusingCount++;
	}

	if (isUnLocked)
	{
		inProc = job.Get();

		if (job.GetRetCode() == DataEmpty)
			downtimeCount++;
		else
		{
			jobCount++;
			isUnLocked = false;
		}
	}

	if (!isUnLocked)
		isUnLocked = jobStream.endJob();
}


void TProc::printResult(void)
{
	cout << endl;
	cout << "The number of jobs received during the simulation process: " << jobStream.getID() << endl;
	cout << "Number of jobs processed by the processor: " << jobCount << endl;
	cout << "The number of jobs received but not processed: " << jobStream.getID() - jobCount << endl;
	cout << "The number of failures in service jobs because of queue overflow: " << refusingCount << endl;
	cout << "The percentage of refusals: " << ((jobStream.getID()) ? floor(refusingCount * 100 / jobStream.getID()) : 0) << "%" << endl;
	cout << "The average number of cycles of the assignment: " << ((jobCount) ? floor((takts - downtimeCount) / jobCount) : 0) << endl;
	cout << "Number of idle cycles: " << downtimeCount << endl;
	cout << "The percentage of downtime: " << ((takts) ? floor(downtimeCount * 100 / takts) : 0) << "%" << endl;
}