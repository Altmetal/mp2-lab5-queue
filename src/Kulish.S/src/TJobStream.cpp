
#include "tjobstream.h"
#include <stdlib.h>
#include <time.h>

using namespace std;

const unsigned short range = 100;

TJobStream::TJobStream(unsigned short _q1, unsigned short _q2) : ID(0), q1(_q1), q2(_q2)
{
	srand(time(NULL));
}

unsigned TJobStream::creatureNewJob(void)
{
	unsigned result = 0;
	if (rand() % range<q1)
		result = ++ID;
	return result;

}

bool TJobStream::endJob(void)
{
	bool result = false;
	if (rand() % range<q2)
		result = true;
	return result;
}

unsigned TJobStream::getID(void)
{
	return ID;
}
